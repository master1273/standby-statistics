package apps.wiceo.standbystatistics.activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.view.Menu
import android.view.MenuItem
import apps.wiceo.standbystatistics.R
import apps.wiceo.standbystatistics.R.font.muli_black
import apps.wiceo.standbystatistics.event.ServiceStateEvent
import apps.wiceo.standbystatistics.service.AppService
import apps.wiceo.standbystatistics.service.CheckServiceJobScheduler
import apps.wiceo.standbystatistics.util.ServiceUtil
import apps.wiceo.standbystatistics.util.SharedPrefUtil
import kotlinx.android.synthetic.main.activity_main.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

class MainActivity : BaseActivity() {

    override fun getLayout(): Int = R.layout.activity_main

    private fun onStartClicked(){
        val serviceRunning = ServiceUtil.isServiceRunning(this, AppService::class.java)
        if (!serviceRunning){
            AppService.start(this)
            CheckServiceJobScheduler.setJob(this)
        } else {
            AppService.stop(this)
            CheckServiceJobScheduler.cancelJob(this)
        }
    }

    private fun setupViews(){
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        btnStart.typeface = ResourcesCompat.getFont(this, muli_black)
        btnStart.setOnClickListener { onStartClicked() }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViews()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

   override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_statistics -> startActivity(Intent(this, StatisticsActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        setUiState(ServiceUtil.isServiceRunning(this, AppService::class.java))
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    private fun setUiState(serviceStarted: Boolean) {
        btnStart.isChecked = serviceStarted

        monitoringStatus.text = when (serviceStarted) {
            true -> getString(R.string.monitoring_active)
            false -> getString(R.string.monitoring_inactive)
        }

        lastMonitoringTime.text = getTimeToSet(serviceStarted)
    }

    private fun getTimeToSet(serviceStarted: Boolean) : String {
        val timeToSet = when(serviceStarted) {
            true -> SharedPrefUtil.getLastStartTime()
            false -> SharedPrefUtil.getLastStopTime()
        }

        if (timeToSet == 0L){
            return when(serviceStarted) {
                true -> getString(R.string.last_start_never)
                false -> getString(R.string.last_start_never)
            }
        }

        val timeStamp = DateTime(timeToSet)
        val today = DateTime()

        if (timeStamp.withTimeAtStartOfDay().isEqual(today.withTimeAtStartOfDay())){
            return when(serviceStarted){
                true -> {
                    val formatter = DateTimeFormat.forPattern(getString(R.string.last_start_today))
                    formatter.print(timeStamp)
                }
                false -> {
                    val formatter = DateTimeFormat.forPattern(getString(R.string.last_stop_today))
                    formatter.print(timeStamp)
                }
            }
        } else {
            return when(serviceStarted){
                true -> {
                    val formatter = DateTimeFormat.forPattern(getString(R.string.last_start))
                    formatter.print(timeStamp)
                }
                false -> {
                    val formatter = DateTimeFormat.forPattern(getString(R.string.last_stop))
                    formatter.print(timeStamp)
                }
            }
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: ServiceStateEvent) {
        setUiState(event.started)
    }
}
