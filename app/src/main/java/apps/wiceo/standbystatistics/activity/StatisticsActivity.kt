package apps.wiceo.standbystatistics.activity

import android.view.View.GONE
import android.view.View.VISIBLE
import apps.wiceo.standbystatistics.R
import apps.wiceo.standbystatistics.adapter.StatisticsAdapter
import apps.wiceo.standbystatistics.data.HistoryRecordDao
import apps.wiceo.standbystatistics.data.StandbyRecordDao
import kotlinx.android.synthetic.main.activity_statistics.*

/**
 * Created by Paul Chernenko
 * on 17.11.2017.
 */
class StatisticsActivity: BaseActivity() {

    override fun getLayout(): Int = R.layout.activity_statistics

    var position = 0

    private fun setupViews(){
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        val list = StandbyRecordDao.loadAllRecords()
        if (list == null || list.isEmpty())
            noDataPlaceholder.visibility = VISIBLE
        else
            noDataPlaceholder.visibility = GONE

        statisticsPager.offscreenPageLimit = 3
        statisticsPager.adapter = StatisticsAdapter(supportFragmentManager)

        tabLayout.setupWithViewPager(statisticsPager)

        @Suppress("UsePropertyAccessSyntax")
        statisticsPager.setCurrentItem(position)
    }

    override fun onStart() {
        setInterstitialUnitId(getString(R.string.ads_interstitial_statistics))
        super.onStart()
        HistoryRecordDao.updateHistory()
        setupViews()
    }

    override fun onStop() {
        position = statisticsPager.currentItem
        super.onStop()
    }
}