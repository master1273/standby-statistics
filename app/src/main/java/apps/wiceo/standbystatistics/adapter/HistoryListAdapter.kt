package apps.wiceo.standbystatistics.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import apps.wiceo.standbystatistics.R

import apps.wiceo.standbystatistics.application.StandbyApp
import apps.wiceo.standbystatistics.data.HistoryRecord
import apps.wiceo.standbystatistics.util.TimeUtil
import kotlinx.android.synthetic.main.item_history.view.*
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

/**
 * Created by Paul Chernenko
 * on 19.11.2017.
 */
class HistoryListAdapter(private val historyList: List<HistoryRecord>): RecyclerView.Adapter<HistoryListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_history, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: HistoryListAdapter.ViewHolder, position: Int) {
        holder.bindItems(historyList[position])
    }

    override fun getItemCount(): Int = historyList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(record: HistoryRecord) {
            val formatter: DateTimeFormatter = DateTimeFormat.forPattern("EEEE, dd MMMM")
            itemView.date.text = formatter.print(record.date)
            itemView.duration.text = StandbyApp.getAppContext().resources.getString(R.string.sleep_duration)
                    .format(TimeUtil.millisecondsToString(record.duration))
            itemView.onCounts.text = StandbyApp.getAppContext().resources.getQuantityString(R.plurals.countOfScreenOns, record.onCounts, record.onCounts)
        }
    }
}