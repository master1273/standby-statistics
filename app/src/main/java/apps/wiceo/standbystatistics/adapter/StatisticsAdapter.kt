package apps.wiceo.standbystatistics.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import apps.wiceo.standbystatistics.R
import apps.wiceo.standbystatistics.application.StandbyApp
import apps.wiceo.standbystatistics.fragment.DailyFragment
import apps.wiceo.standbystatistics.fragment.HistoryFragment
import apps.wiceo.standbystatistics.fragment.HourlyFragment

/**
 * Created by Paul Chernenko
 * on 17.11.2017.
 */
class StatisticsAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    override fun getCount(): Int = 3

    override fun getItem(position: Int): Fragment = when (position) {
        0 -> DailyFragment.newInstance()
        1 -> HourlyFragment.newInstance()
        2 -> HistoryFragment.newInstance()
        else -> throw IllegalArgumentException("Unknown page position: " + position)
    }

    override fun getPageTitle(position: Int): CharSequence = when (position) {
        0 -> StandbyApp.getAppContext().resources.getString(R.string.daily)
        1 -> StandbyApp.getAppContext().resources.getString(R.string.hourly)
        2 -> StandbyApp.getAppContext().resources.getString(R.string.history)
        else -> throw IllegalArgumentException("Unknown page position: " + position)
    }
}
