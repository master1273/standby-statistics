package apps.wiceo.standbystatistics.application

import android.app.Application
import android.content.Context
import apps.wiceo.standbystatistics.R
import apps.wiceo.standbystatistics.common.Constants
import com.activeandroid.ActiveAndroid
import net.danlew.android.joda.JodaTimeAndroid
import com.crashlytics.android.Crashlytics
import com.google.android.gms.ads.MobileAds
import io.fabric.sdk.android.Fabric
import org.solovyev.android.checkout.Billing
import org.solovyev.android.checkout.Cache


/**
 * Created by Paul Chernenko
 * on 17.11.2017.
 */
class StandbyApp: Application() {

    val mBilling = Billing(this, object : Billing.DefaultConfiguration() {
        override fun getPublicKey(): String = Constants.IN_APP_BILLING_KEY
        override fun getCache(): Cache = Billing.newCache()
    })

    init {
        instance = this
    }

    companion object {
        var instance: StandbyApp? = null

        fun getAppContext() : Context = instance!!.applicationContext
    }

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        ActiveAndroid.initialize(this)
        JodaTimeAndroid.init(this)
        MobileAds.initialize(applicationContext, getString(R.string.ads_app_id))
    }
}