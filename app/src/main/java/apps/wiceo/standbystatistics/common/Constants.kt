package apps.wiceo.standbystatistics.common

/**
 * Created by Paul Chernenko
 * on 19.11.2017.
 */
class Constants {
    companion object {
        const val DAILY_DATA_DAYS = 7
        const val ADD_DISPLAY_PERIOD = 3
        const val USE_TEST_ADS = false
        const val IN_APP_BILLING_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzlsQhtzHoCnpnnRXT/mk82HKuakswmi5mfoak3K/mhyRleAUQ5TFWPWLmJx5CnyylwuVMN6KF4AullemyLKdONKpERiG1gEKeJTIlWN6a1KEWn1e8azBNMq24uvMulfTQTHWpgLTqLf7FNvQGSo4zX+XA+cHYJxwRaGnd2kiMsKHFGiuXFrBZ1xJAjo65/BibOFRfPc8YPcP5G3/Ug8F80gm4WQ4LQ7VQPY5e2+EeGjd8UgFFwDnh5/xncU5L7U2xznxW8s8AWKnjfMVyfakYQty6ZQvcwJdLd8jnYCqVLs77qIxdQ2CmJlJjMiIlx5JppgVd2nJbvx6GV+kLD8plwIDAQAB"
        const val IN_APP_ID = "standby_stats_ads_free_01"
        //"android.test.purchased" - id for testing
    }
}