package apps.wiceo.standbystatistics.component

import android.content.Context
import android.support.design.widget.TabLayout
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.view.ViewPager
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import android.view.ViewGroup

import apps.wiceo.standbystatistics.R
import apps.wiceo.standbystatistics.application.StandbyApp

/**
 * Created by Paul Chernenko
 * on 17.11.2017.
 */

class CustomTabLayout : TabLayout {
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun setupWithViewPager(viewPager: ViewPager?) {
        super.setupWithViewPager(viewPager)

        val typeface = ResourcesCompat.getFont(StandbyApp.getAppContext(), R.font.muli_regular)

        this.removeAllTabs()

        val slidingTabStrip = getChildAt(0) as ViewGroup

        var i = 0
        val count = viewPager!!.adapter.count
        while (i < count) {
            val tab = this.newTab()
            this.addTab(tab.setText(viewPager.adapter.getPageTitle(i)))
            val view = (slidingTabStrip.getChildAt(i) as ViewGroup).getChildAt(1) as AppCompatTextView
            view.typeface = typeface
            i++
        }
    }
}