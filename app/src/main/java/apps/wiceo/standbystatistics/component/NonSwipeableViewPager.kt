package apps.wiceo.standbystatistics.component

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

/**
 * Created by Paul Chernenko
 * on 18.11.2017.
 */
class NonSwipeableViewPager : ViewPager {
    private var isSwipeable = false

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    override fun onInterceptTouchEvent(arg0: MotionEvent): Boolean {
        return if (isSwipeable)
            super.onInterceptTouchEvent(arg0)
        else
            false
    }
}