package apps.wiceo.standbystatistics.data

import com.activeandroid.Model
import com.activeandroid.annotation.Column
import com.activeandroid.annotation.Table

/**
 * Created by Paul Chernenko
 * on 19.11.2017.
 */

@Table(name = "HistoryRecords")
class HistoryRecord: Model {

    @Column(name = "date")
    var date: Long = 0L

    @Column(name = "duration")
    var duration: Long = 0L

    @Column(name = "onCounts")
    var onCounts: Int = 0

    constructor(date: Long, duration: Long, onCounts: Int){
        this.date = date
        this.duration = duration
        this.onCounts = onCounts
    }

    constructor()
}
