package apps.wiceo.standbystatistics.data

import apps.wiceo.standbystatistics.common.Constants
import apps.wiceo.standbystatistics.util.TimeUtil
import com.activeandroid.query.Select
import org.joda.time.DateTime

/**
 * Created by Paul Chernenko
 * on 19.11.2017.
 */

class HistoryRecordDao{
    companion object {
        fun createRecord(date: Long, duration: Long, onCount: Int): HistoryRecord {
            val historyRecord = HistoryRecord(date, duration, onCount)
            saveRecord(historyRecord)
            return historyRecord
        }

        fun getLastRecord(): HistoryRecord? = Select().from(HistoryRecord::class.java).orderBy("id DESC").executeSingle()

        private fun saveRecord(historyRecord: HistoryRecord) = historyRecord.save()!!

        fun loadAllRecords(): List<HistoryRecord>? = Select().from(HistoryRecord::class.java).execute()

        fun getRecordById(recordId: Long): HistoryRecord? = Select().from(HistoryRecord::class.java).where("id = ?", recordId).executeSingle()

        private fun getRecordByDate(date: Long): HistoryRecord? = Select().from(HistoryRecord::class.java).where("date = ?", date).executeSingle()

        fun updateHistory(){
            for (data in StandbyRecordDao.getDailyData()){
                var record = getRecordByDate(data.key)
                if (record == null){
                    record = HistoryRecord(data.key, data.value, StandbyRecordDao.getScreenOns(data.key))
                    saveRecord(record)
                } else if (record.date == TimeUtil.getStartOfDay(DateTime().millis)){
                    record.onCounts = StandbyRecordDao.getScreenOns(data.key)
                    record.duration = when{
                        data.value.toInt() % TimeUtil.HOUR != 0 && data.value.toInt() != 0 -> data.value + 1000
                        else -> data.value
                    }
                    saveRecord(record)
                }
            }
            StandbyRecordDao.deleteRecords(Constants.DAILY_DATA_DAYS)
        }
    }
}
