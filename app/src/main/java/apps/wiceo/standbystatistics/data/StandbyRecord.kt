package apps.wiceo.standbystatistics.data

import com.activeandroid.Model
import com.activeandroid.annotation.Column
import com.activeandroid.annotation.Table

/**
 * Created by Paul Chernenko
 * on 17.11.2017.
 */
@Table(name = "StandbyRecords")
class StandbyRecord: Model{

    @Column(name = "start_time")
    var startTime : Long = 0L

    @Column(name = "end_time")
    var endTime : Long = 0L

    constructor(startTime: Long, endTime: Long) {
        this.startTime = startTime
        this.endTime = endTime
    }

    constructor()
}