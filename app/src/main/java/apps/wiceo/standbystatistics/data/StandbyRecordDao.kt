package apps.wiceo.standbystatistics.data

import apps.wiceo.standbystatistics.util.TimeUtil
import com.activeandroid.query.Delete
import com.activeandroid.query.Select
import org.joda.time.DateTime

/**
 * Created by Paul Chernenko
 * on 17.11.2017.
 */
class StandbyRecordDao{
    companion object {
        fun createRecord(startTime: Long, endTime: Long): StandbyRecord {
            val standbyRecord = StandbyRecord(startTime, endTime)
            saveRecord(standbyRecord)
            return standbyRecord
        }

        fun getLastRecord(): StandbyRecord? = Select().from(StandbyRecord::class.java).orderBy("id DESC").executeSingle()

        fun saveRecord(standbyRecord: StandbyRecord) = standbyRecord.save()!!

        fun loadAllRecords(): List<StandbyRecord>? = Select().from(StandbyRecord::class.java).execute()

        private fun loadAllRecords(start: Long, end: Long): List<StandbyRecord>?
                = Select().from(StandbyRecord::class.java)
                .where("(end_time > $start AND start_time < $end)")
                .execute()

        fun getScreenOns(timestamp: Long): Int {
            val list: List<StandbyRecord>? = Select().from(StandbyRecord::class.java)
                    .where("(end_time >= ${TimeUtil.getStartOfDay(timestamp)} AND end_time <= ${TimeUtil.getEndOfDay(timestamp)})")
                    .execute()

            return if (list != null)
                list.size
            else
                0
        }

        fun getRecordById(recordId: Long): StandbyRecord? = Select().from(StandbyRecord::class.java).where("id = $recordId").executeSingle()

        fun deleteRecords(daysToKeep: Int) {
            val today = DateTime().minusDays(daysToKeep)
            Delete().from(StandbyRecord::class.java)
                    .where("end_time < ${today.millis}")
                    .execute<StandbyRecord>()
        }

        private fun deleteRecord(standbyRecord: StandbyRecord) {
            standbyRecord.delete()
        }

        fun getDailyData(): Map<Long, Long>{
            val allRecords: List<StandbyRecord>? = loadAllRecords()
            val dailyData = mutableMapOf<Long, Long>()

            if (!(allRecords == null || allRecords.isEmpty())){
                for (record in allRecords){
                    if (record.startTime == 0L || record.endTime == 0L){
                        StandbyRecordDao.deleteRecord(record)
                        continue
                    }

                    var startOfDay = TimeUtil.getStartOfDay(record.startTime)
                    var endOfDay = TimeUtil.getEndOfDay(record.startTime)

                    while (record.endTime > endOfDay){
                        if (!dailyData.containsKey(startOfDay))
                            dailyData.put(startOfDay, 0)

                        if (startOfDay != TimeUtil.getStartOfDay(record.startTime))
                            dailyData[startOfDay] = dailyData[startOfDay]!! + endOfDay - startOfDay
                        else
                            dailyData[startOfDay] = dailyData[startOfDay]!! + endOfDay - record.startTime

                        startOfDay = TimeUtil.getStartOfNextDay(startOfDay)
                        endOfDay = TimeUtil.getEndOfNextDay(endOfDay)
                    }

                    if (!dailyData.containsKey(startOfDay))
                        dailyData.put(startOfDay, 0)

                    if (startOfDay != TimeUtil.getStartOfDay(record.startTime))
                        dailyData[startOfDay] = dailyData[startOfDay]!! + record.endTime - startOfDay
                    else
                        dailyData[startOfDay] = dailyData[startOfDay]!! + record.endTime - record.startTime
                }
            }
            return dailyData
        }

        fun getHourlyData(day: DateTime): Map<Long, Long>{
            val allRecords: List<StandbyRecord>?
                    = loadAllRecords(TimeUtil.getStartOfDay(day.millis), TimeUtil.getEndOfDay(day.millis))
            val hourlyData = mutableMapOf<Long, Long>()

            if (!(allRecords == null || allRecords.isEmpty())){
                var timeInstance = TimeUtil.getStartOfDay(day.millis)
                var limit = TimeUtil.getStartOfNextDay(day.millis)
                if (TimeUtil.getStartOfDay(day.millis) == TimeUtil.getStartOfDay(DateTime().millis))
                    limit = day.millis
                while (timeInstance < limit){
                    hourlyData.put(timeInstance, 0)
                    timeInstance = TimeUtil.getStartOfNextHour(timeInstance)
                }

                for(record in allRecords){
                    if (record.startTime == 0L || record.endTime == 0L){
                        StandbyRecordDao.deleteRecord(record)
                        continue
                    }

                    val currentData = mutableMapOf<Long, Long>()

                    var startOfHour = TimeUtil.getStartOfHour(record.startTime)
                    while(record.startTime < record.endTime){
                        if (!currentData.contains(startOfHour))
                            currentData.put(startOfHour, 0)

                        if (record.endTime < TimeUtil.getEndOfHour(record.startTime))
                            currentData[startOfHour] = currentData[startOfHour]!! + record.endTime - record.startTime
                        else
                            currentData[startOfHour] = currentData[startOfHour]!! + TimeUtil.getEndOfHour(record.startTime) - record.startTime

                        record.startTime = TimeUtil.getStartOfNextHour(record.startTime)
                        startOfHour = TimeUtil.getStartOfHour(record.startTime)
                    }

                    for (v in currentData){
                        if (hourlyData.containsKey(v.key))
                            hourlyData[v.key] = hourlyData[v.key]!! + v.value

                    }
                }
            }
            return hourlyData
        }
    }
}