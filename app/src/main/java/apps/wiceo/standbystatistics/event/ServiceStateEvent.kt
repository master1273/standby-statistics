package apps.wiceo.standbystatistics.event

/**
 * Created by Paul Chernenko
 * on 17.11.2017.
 */
class ServiceStateEvent(val started: Boolean)