package apps.wiceo.standbystatistics.fragment

import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import apps.wiceo.standbystatistics.R
import apps.wiceo.standbystatistics.data.StandbyRecordDao
import apps.wiceo.standbystatistics.util.TimeUtil
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.utils.ViewPortHandler
import kotlinx.android.synthetic.main.fragment_daily.*
import org.joda.time.format.DateTimeFormat
import java.util.*

/**
 * Created by Paul Chernenko
 * on 17.11.2017.
 */
class DailyFragment: Fragment() {
    companion object {
        fun newInstance() = DailyFragment()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater?.inflate(R.layout.fragment_daily, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupGraph()
    }

    private fun setupGraph(){
        val chartData: Map<Long, Long> = StandbyRecordDao.getDailyData()
        val chartDataList = mutableListOf<Data>()

        var i = 0F
        for (value in chartData){
            chartDataList.add(Data(i, value.key, value.value.toFloat(), ""))
            i++
        }

        setupChart(chartDataList)
    }

    private fun setupChart(data: List<Data>) {

        val labelColor = ContextCompat.getColor(activity, R.color.colorPrimaryDark)

        try {
            chart.clearValues()
            chart.clear()
        } catch (ignored: NullPointerException){}

        chart.setBackgroundColor(Color.WHITE)

        chart.extraRightOffset = 10f
        chart.extraLeftOffset = 10f
        chart.extraBottomOffset = 10f
        chart.extraTopOffset = 10f

        chart.setDrawBarShadow(false)
        chart.setDrawValueAboveBar(true)

        chart.description.isEnabled = false
        chart.legend.isEnabled = false

        chart.setPinchZoom(false)
        chart.isDoubleTapToZoomEnabled = false
        chart.isScaleXEnabled = false
        chart.isScaleYEnabled = false
        chart.setTouchEnabled(true)

        chart.setDrawGridBackground(false)

        chart.axisLeft.setValueFormatter { value, _ -> (String.format("%.1fh", value/TimeUtil.HOUR)) }
        chart.axisLeft.granularity = 1f
        chart.axisLeft.setDrawZeroLine(true)
        chart.axisLeft.axisMinimum = 0f
        chart.axisLeft.setDrawAxisLine(false)
        chart.axisLeft.textColor = labelColor
        chart.axisLeft.textSize = 14f
        chart.axisLeft.typeface = ResourcesCompat.getFont(activity, R.font.muli_bold)
        chart.axisRight.isEnabled = false

        chart.xAxis.setValueFormatter { value, _ -> data[Math.min(Math.max(value.toInt(), 0), data.size - 1)].xAxisValue }
        chart.xAxis.setDrawGridLines(false)
        chart.xAxis.granularity = 1F
        chart.xAxis.textColor = labelColor
        chart.xAxis.textSize = 14f
        chart.xAxis.typeface = ResourcesCompat.getFont(activity, R.font.muli_bold)

        chart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        chart.xAxis.labelCount = data.size

        setData(data)

        val visibilityMax = when{
            data.size < 4 -> data.size.toFloat()
            else -> 4F
        }

        chart.setVisibleXRange(1F, visibilityMax)
        chart.invalidate()
        if (data.isNotEmpty())
            chart.moveViewToX(data.last().xValue)
    }

    private fun setData(dataList: List<Data>) {

        val darkColor = ContextCompat.getColor(activity, R.color.colorPrimaryDark)

        val values = dataList.map { BarEntry(it.xValue, it.yValue) }

        val set: BarDataSet

        if (chart.data != null && chart.data.dataSetCount > 0) {
            set = chart.data.getDataSetByIndex(0) as BarDataSet
            set.values = values
            chart.data.notifyDataChanged()
            chart.notifyDataSetChanged()
        } else {
            set = BarDataSet(values, "Values")
            set.color = darkColor
            set.valueTextColor = darkColor

            val data = BarData(set)
            data.setValueTextSize(18f)
            data.setValueTypeface(ResourcesCompat.getFont(activity, R.font.muli_bold))
            data.setValueFormatter(ValueFormatter())
            data.barWidth = 0.8f

            chart.data = data
            chart.invalidate()
        }
    }

    private inner class Data internal constructor(
            internal val xValue: Float,
            date: Long,
            internal val yValue: Float,
            internal var xAxisValue: String = "") {

        init {
            val formatter = DateTimeFormat.forPattern("MM-dd")
            xAxisValue = formatter.print(date)
        }
    }

    private inner class ValueFormatter : IValueFormatter {

        override fun getFormattedValue(v: Float, entry: Entry, dataSetIndex: Int, viewPortHandler: ViewPortHandler): String {

            val value = when{
                v.toInt() % TimeUtil.HOUR != 0 && v.toInt() != 0 -> v.toLong() + 1000
                else -> v.toLong()
            }

            return when {
                value >= TimeUtil.HOUR -> String.format(Locale.US, "%dh\n%02dm", (value / TimeUtil.HOUR), (value % TimeUtil.HOUR / TimeUtil.MINUTE))
                value >= TimeUtil.MINUTE -> String.format(Locale.US, "%dm\n%02ds", (value / TimeUtil.MINUTE), (value % TimeUtil.MINUTE / TimeUtil.SECOND))
                else -> String.format(Locale.US, "%ds", (value / TimeUtil.SECOND))
            }
        }
    }
}