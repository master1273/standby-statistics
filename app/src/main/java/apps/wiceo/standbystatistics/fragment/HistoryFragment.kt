package apps.wiceo.standbystatistics.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import apps.wiceo.standbystatistics.R
import apps.wiceo.standbystatistics.adapter.HistoryListAdapter
import apps.wiceo.standbystatistics.component.DividerItemDecoration
import apps.wiceo.standbystatistics.data.HistoryRecordDao
import kotlinx.android.synthetic.main.fragment_history.*

/**
 * Created by Paul Chernenko
 * on 19.11.2017.
 */
class HistoryFragment: Fragment(){
    companion object {
        fun newInstance() = HistoryFragment()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater?.inflate(R.layout.fragment_history, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val list = HistoryRecordDao.loadAllRecords()
        if (list != null) {
            val linearLayoutManager = LinearLayoutManager(activity)
            linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
            historyList.layoutManager = linearLayoutManager
            historyList.setHasFixedSize(true)
            historyList.addItemDecoration(DividerItemDecoration(activity))

            historyList.adapter = HistoryListAdapter(list.sortedByDescending { it.date })
        }
    }
}