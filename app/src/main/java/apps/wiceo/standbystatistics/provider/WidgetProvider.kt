package apps.wiceo.standbystatistics.provider

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import apps.wiceo.standbystatistics.R
import apps.wiceo.standbystatistics.activity.StatisticsActivity
import apps.wiceo.standbystatistics.application.StandbyApp
import apps.wiceo.standbystatistics.data.StandbyRecord
import apps.wiceo.standbystatistics.data.StandbyRecordDao
import apps.wiceo.standbystatistics.service.AppService
import apps.wiceo.standbystatistics.util.ServiceUtil
import apps.wiceo.standbystatistics.util.TimeUtil


/**
 * Created by Paul Chernenko
 * on 17.11.2017.
 */
class WidgetProvider: AppWidgetProvider() {

    override fun onUpdate(context: Context?, appWidgetManager: AppWidgetManager?, appWidgetIds: IntArray) {
        val count = appWidgetIds.size

        for (i in 0 until count){
            val widgetId = appWidgetIds[i]
            val remoteViews = RemoteViews(context?.packageName,
                    R.layout.app_widget)
            remoteViews.setTextViewText(R.id.status, when(ServiceUtil.isServiceRunning(StandbyApp.getAppContext(), AppService::class.java)){
                true -> {StandbyApp.getAppContext().resources.getString(R.string.active)}
                false -> {StandbyApp.getAppContext().resources.getString(R.string.inactive)}
            })

            remoteViews.setTextViewText(R.id.lastSleepDuration, getLastSleepText())

            val intent = Intent(context, StatisticsActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(context,
                    0, intent,0)
            remoteViews.setOnClickPendingIntent(R.id.widgetView, pendingIntent)
            appWidgetManager?.updateAppWidget(widgetId, remoteViews)
        }
    }

    private fun getLastSleepText(): String{
        val dbRecord : StandbyRecord? = StandbyRecordDao.getLastRecord()
        return if (dbRecord != null){
            val difference = dbRecord.endTime.minus(dbRecord.startTime)
            return if (difference > 0){
                String.format(StandbyApp.getAppContext().resources.getString(R.string.last_sleep_duration), TimeUtil.millisecondsToString(difference))
            } else {
                String.format(StandbyApp.getAppContext().resources.getString(R.string.last_sleep_duration), "0")
            }
        } else {
            String.format(StandbyApp.getAppContext().resources.getString(R.string.last_sleep_duration),
                    StandbyApp.getAppContext().resources.getString(R.string.no_data))
        }
    }
}