package apps.wiceo.standbystatistics.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.text.TextUtils
import apps.wiceo.standbystatistics.application.StandbyApp

/**
 * Created by Paul Chernenko
 * on 17.11.2017.
 */
class ScreenMonitor(private val listener: ScreenStateListener) : BroadcastReceiver() {

    fun register() {
        val intentFilter = IntentFilter()
        intentFilter.addAction(Intent.ACTION_SCREEN_ON)
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF)
        StandbyApp.getAppContext().registerReceiver(this, intentFilter)
    }

    fun unregister() {
        StandbyApp.getAppContext().unregisterReceiver(this)
    }

    override fun onReceive(context: Context, intent: Intent) {
        if (TextUtils.equals(intent.action, Intent.ACTION_SCREEN_ON)) {
            listener.screenStateChanged(true)
        } else {
            listener.screenStateChanged(false)
        }
    }

    interface ScreenStateListener {
        fun screenStateChanged(isScreenOn: Boolean)
    }
}