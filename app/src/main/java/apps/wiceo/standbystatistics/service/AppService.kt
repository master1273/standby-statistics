package apps.wiceo.standbystatistics.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.os.PowerManager
import apps.wiceo.standbystatistics.data.StandbyRecordDao
import apps.wiceo.standbystatistics.event.ServiceStateEvent
import apps.wiceo.standbystatistics.receiver.ScreenMonitor
import apps.wiceo.standbystatistics.util.SharedPrefUtil
import apps.wiceo.standbystatistics.util.WidgetUtil.Companion.updateWidget
import org.greenrobot.eventbus.EventBus

/**
 * Created by Paul Chernenko
 * on 17.11.2017.
 */
class AppService: Service(), ScreenMonitor.ScreenStateListener {

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, AppService::class.java)
            context.startService(intent)
            EventBus.getDefault().post(ServiceStateEvent(true))
            updateWidget()
        }

        fun stop(context: Context) {
            SharedPrefUtil.setCurrentSession(0)
            val intent = Intent(context, AppService::class.java)
            context.stopService(intent)
            EventBus.getDefault().post(ServiceStateEvent(false))
            updateWidget()
        }
    }

    private val screenMonitor = ScreenMonitor(this)

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int = Service.START_STICKY

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onCreate() {
        super.onCreate()
        screenMonitor.register()
        SharedPrefUtil.setLastStartTime(System.currentTimeMillis())
        EventBus.getDefault().post(ServiceStateEvent(true))
        updateWidget()

        val pm = getSystemService(Context.POWER_SERVICE) as PowerManager
        if (if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            !pm.isInteractive
        } else {
            @Suppress("DEPRECATION")
            !pm.isScreenOn
        }) {
            val sessionId = SharedPrefUtil.getCurrentSession()
            val oldRecord = StandbyRecordDao.getRecordById(sessionId)
            if (oldRecord == null){
                val id = StandbyRecordDao.createRecord(System.currentTimeMillis(), 0).id
                SharedPrefUtil.setCurrentSession(id)
            }
        } else {
            val sessionId = SharedPrefUtil.getCurrentSession()
            val oldRecord = StandbyRecordDao.getRecordById(sessionId)
            if (oldRecord != null && oldRecord.endTime == 0L && oldRecord.startTime != 0L){
                oldRecord.endTime = System.currentTimeMillis()
                StandbyRecordDao.saveRecord(oldRecord)
            }
            SharedPrefUtil.setCurrentSession(0)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        screenMonitor.unregister()
        SharedPrefUtil.setLastStopTime(System.currentTimeMillis())
        EventBus.getDefault().post(ServiceStateEvent(false))
        updateWidget()
    }

    override fun screenStateChanged(isScreenOn: Boolean) {
        if (isScreenOn) {
            val id = SharedPrefUtil.getCurrentSession()
            val record = StandbyRecordDao.getRecordById(id)
            if (record != null){
                record.endTime = System.currentTimeMillis()
                StandbyRecordDao.saveRecord(record)
            }
            SharedPrefUtil.setCurrentSession(0)
            updateWidget()
        } else {
            //Triggered when screen goes off
            val id = StandbyRecordDao.createRecord(System.currentTimeMillis(), 0).id
            SharedPrefUtil.setCurrentSession(id)
        }
    }
}