package apps.wiceo.standbystatistics.service

import android.content.Context
import com.firebase.jobdispatcher.*

/**
 * Created by Paul Chernenko
 * on 08.11.2017.
 */

class CheckServiceJobScheduler {

    companion object {
        private const val JOB_TAG = "check-service-job-scheduler"
        private const val INTERVAL_IN_SECONDS = 5 * 60

        fun setJob(context: Context) {
            val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(context))

            val myJob = dispatcher.newJobBuilder()
                    .setService(CheckServiceJobService::class.java)
                    .setTag(JOB_TAG)
                    .setRecurring(true)
                    .setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                    .setTrigger(Trigger.executionWindow((INTERVAL_IN_SECONDS * 0.9).toInt(), INTERVAL_IN_SECONDS))
                    .setReplaceCurrent(false)
                    .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                    .build()

            dispatcher.mustSchedule(myJob)
        }

        fun cancelJob(context: Context) {
            val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(context))
            dispatcher.cancel(JOB_TAG)
        }
    }
}
