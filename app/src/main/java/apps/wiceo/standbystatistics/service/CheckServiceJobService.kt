package apps.wiceo.standbystatistics.service

import apps.wiceo.standbystatistics.util.ServiceUtil
import com.firebase.jobdispatcher.JobParameters
import com.firebase.jobdispatcher.JobService

/**
 * Created by Paul Chernenko
 * on 08.11.2017.
 */

class CheckServiceJobService : JobService() {

    override fun onStartJob(params: JobParameters): Boolean {
        if (!ServiceUtil.isServiceRunning(applicationContext, AppService::class.java))
            AppService.start(applicationContext)
        return false
    }

    override fun onStopJob(params: JobParameters): Boolean = false

}
