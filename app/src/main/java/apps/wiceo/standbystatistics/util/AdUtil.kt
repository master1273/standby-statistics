package apps.wiceo.standbystatistics.util

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings
import apps.wiceo.standbystatistics.common.Constants
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd

/**
 * Created by Paul Chernenko
 * on 20.11.2017.
 */
class AdUtil {
    companion object {
        fun isTestDevice(context: Context): Boolean =
                java.lang.Boolean.valueOf(Settings.System.getString(context.contentResolver, "firebase.test.lab"))!!

        fun getAdRequest(context: Context): AdRequest {
            val builder = AdRequest.Builder()
            val deviceId = getTestDeviceId(context)

            if (Constants.USE_TEST_ADS && deviceId != null)
                builder.addTestDevice(deviceId)

            return builder.build()
        }

        fun getInterstitialAd(context: Context, adUnitId: String): InterstitialAd {
            val interstitialAd = InterstitialAd(context)
            interstitialAd.adUnitId = adUnitId

            return interstitialAd

        }

        @SuppressLint("HardwareIds")
        private fun getTestDeviceId(context: Context): String? {
            @SuppressLint("HardwareIds")
            var androidId: String? = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
            androidId = md5(androidId)
            return if (androidId != null) {
                androidId.toUpperCase()
            } else null
        }

        private fun md5(md5: String?): String? {
            try {
                val md = java.security.MessageDigest.getInstance("MD5")
                val array = md.digest(md5!!.toByteArray())
                val sb = StringBuilder()
                for (anArray in array) {
                    sb.append(Integer.toHexString(anArray.toInt() and 0xFF or 0x100).substring(1, 3))
                }
                return sb.toString()
            } catch (ignored: java.security.NoSuchAlgorithmException) {
            }

            return null
        }
    }
}