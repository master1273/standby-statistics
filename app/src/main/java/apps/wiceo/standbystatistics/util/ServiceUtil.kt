package apps.wiceo.standbystatistics.util

import android.app.ActivityManager
import android.content.Context

/**
 * Created by Paul Chernenko
 * on 17.11.2017.
 */
class ServiceUtil{
    companion object {

        @Suppress("DEPRECATION")
        fun isServiceRunning(context: Context, serviceClass: Class<*>): Boolean {
            val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            return manager.getRunningServices(Integer.MAX_VALUE).any { serviceClass.name == it.service.className }
        }
    }
}