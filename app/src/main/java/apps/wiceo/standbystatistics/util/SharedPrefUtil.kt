package apps.wiceo.standbystatistics.util

import android.content.SharedPreferences
import android.preference.PreferenceManager
import apps.wiceo.standbystatistics.application.StandbyApp

/**
 * Created by Paul Chernenko
 * on 17.11.2017.
 */
class SharedPrefUtil{
    companion object {
        private const val CURRENT_SESSION_ID = "currentSessionId"
        private const val LAST_START_TIME = "lastStartTime"
        private const val LAST_STOP_TIME = "lastStopTime"


        fun getSharedPreferences(): SharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(StandbyApp.getAppContext())

        /***********************************************/
        fun setCurrentSession(id: Long) {
            putLong(CURRENT_SESSION_ID, id)
        }

        fun setLastStartTime(time: Long){
            putLong(LAST_START_TIME, time)
        }

        fun setLastStopTime(time: Long){
            putLong(LAST_STOP_TIME, time)
        }

        /***********************************************/
        fun getCurrentSession(): Long = getLong(CURRENT_SESSION_ID)

        fun getLastStartTime(): Long = getLong(LAST_START_TIME)

        fun getLastStopTime(): Long = getLong(LAST_STOP_TIME)

        /***********************************************/
        private fun putLong(key: String, value: Long) {
            val editor = getSharedPreferences().edit()
            editor.putLong(key, value)
            editor.apply()
        }

        fun putInt(key: String, value: Int) {
            val editor = getSharedPreferences().edit()
            editor.putInt(key, value)
            editor.apply()
        }

        /***********************************************/
        private fun getLong(key: String): Long = getSharedPreferences().getLong(key, 0)
        fun getInt(key: String): Int = getSharedPreferences().getInt(key, 0)
    }
}