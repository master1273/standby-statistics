package apps.wiceo.standbystatistics.util

import apps.wiceo.standbystatistics.R
import apps.wiceo.standbystatistics.application.StandbyApp
import org.joda.time.DateTime

/**
 * Created by Paul Chernenko
 * on 17.11.2017.
 */
class TimeUtil{
    companion object {
        val SECOND = 1000
        val MINUTE = 60 * SECOND
        val HOUR = 60 * MINUTE

        fun millisecondsToString(difference: Long): String {
            var ms = difference

            var hour = "0"
            var min = "00"
            var sec = "00"

            var minFormat = "%d"
            var secFormat = "%d"


            if (ms > HOUR) {
                hour =  "%d".format(ms / HOUR)
                ms %= HOUR.toLong()
                minFormat = "%02d"
                secFormat = "%02d"
            }
            if (ms > MINUTE) {
                min = minFormat.format(ms / MINUTE)
                ms %= MINUTE.toLong()
                secFormat = "%02d"
            }
            if (ms > SECOND) {
                sec = secFormat.format(ms / SECOND)
            }

            return if (hour == "0")
                if (min == "00")
                    String.format(StandbyApp.getAppContext().resources.getString(R.string.time_s), sec)
                else
                    String.format(StandbyApp.getAppContext().resources.getString(R.string.time_m), min, sec)
            else
                String.format(StandbyApp.getAppContext().resources.getString(R.string.time_h), hour, min, sec)
        }

        fun getStartOfDay(timestamp: Long): Long =
                DateTime(timestamp).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).millis

        fun getEndOfDay(timestamp: Long): Long =
                DateTime(timestamp).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999).millis

        fun getStartOfNextDay(timestamp: Long): Long =
                DateTime(getStartOfDay(timestamp)).plusDays(1).millis

        fun getEndOfNextDay(timestamp: Long): Long =
                DateTime(getEndOfDay(timestamp)).plusDays(1).millis


        fun getStartOfHour(timestamp: Long): Long =
                DateTime(timestamp).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).millis

        fun getEndOfHour(timestamp: Long): Long =
                DateTime(timestamp).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999).millis

        fun getStartOfNextHour(timestamp: Long): Long =
                DateTime(getStartOfHour(timestamp)).plusHours(1).millis

        fun getEndOfNextHour(timestamp: Long): Long =
                DateTime(getEndOfHour(timestamp)).plusHours(1).millis



    }
}