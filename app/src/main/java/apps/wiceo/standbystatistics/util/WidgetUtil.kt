package apps.wiceo.standbystatistics.util

import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Intent
import apps.wiceo.standbystatistics.application.StandbyApp
import apps.wiceo.standbystatistics.provider.WidgetProvider

/**
 * Created by Paul Chernenko
 * on 17.11.2017.
 */
class WidgetUtil{
    companion object {
        fun updateWidget(){
            val context = StandbyApp.getAppContext()
            val intent = Intent(context, WidgetProvider::class.java)
            intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            val ids = AppWidgetManager.getInstance(context).getAppWidgetIds(ComponentName(context, WidgetProvider::class.java))
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
            context.sendBroadcast(intent)
        }
    }
}